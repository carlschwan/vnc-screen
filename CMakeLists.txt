cmake_minimum_required(VERSION 3.14)
project(vncscreen)

set(KF_MIN_VERSION "5.57.0")
set(QT_MIN_VERSION "5.12.0")

find_package(ECM ${KF_MIN_VERSION} REQUIRED NO_MODULE)

set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH})

include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDECompilerSettings NO_POLICY_SCOPE)


find_package(Qt5 ${QT_MIN_VERSION} REQUIRED NO_MODULE COMPONENTS Core Quick Test Gui QuickControls2 Widgets)
find_package(KF5 ${KF_MIN_VERSION} REQUIRED COMPONENTS Kirigami2 I18n CoreAddons)
#find_package(LibVNCServer)
find_library(VNCCLIENT vncclient)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
add_subdirectory("po")
add_subdirectory(src)


feature_summary(WHAT ALL INCLUDE_QUIET_PACKAGES FATAL_ON_MISSING_REQUIRED_PACKAGES)


find_package(KF5I18n CONFIG REQUIRED)
ki18n_install(po)
install(TARGETS vncscreen RUNTIME DESTINATION /usr/bin/)
install(PROGRAMS org.kde.vncscreen.desktop DESTINATION ${KDE_INSTALL_APPDIR})
install(FILES vncscreen.svg DESTINATION ${KDE_INSTALL_FULL_ICONDIR}/hicolor/scalable/apps/)
